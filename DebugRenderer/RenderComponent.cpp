#include "RenderComponent.h"
#include "GameObject.h"

void RenderComponent::Draw()
{
	glm::mat4 modelMat = parent->transform.GetMatrix();
	shader->SetUniformValue("Model", &modelMat);
	//shader->Use();
	model->Draw();
}
