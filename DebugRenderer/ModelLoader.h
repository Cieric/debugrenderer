#pragma once

class Model;

class ModelLoader
{
private:
	ModelLoader();

public:
	static ModelLoader& getInstance();

	Model* loadModel(const char* filepath, const char* basedir);
};