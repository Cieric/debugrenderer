#include "InputManager.h"

Hash InputManager::getHash(DeviceID did, InputID iid)
{
	auto h1 = std::hash<DeviceID>{}(did);
	auto h2 = std::hash<InputID>{}(iid);
	return h1 ^ h2;
}

void InputManager::InputUpdateCallback(DeviceID deviceID, InputID inputID, float val)
{
	stored[getHash(deviceID, inputID)] = val;
}

void InputManager::Update()
{
	for (size_t i = 0; i < pollingDeviceCount; i++)
	{
		DeviceFunction& device = pollingDevices[i];
		device.deviceFunction(device.data, device.id, *this);
	}
}

DeviceID InputManager::AddNonPollingDeviceFunction(void *data, void(*deviceFunction)(void *data, DeviceID, InputManager &))
{
	DeviceID id = (Hash)nonPollingDeviceCount;
	nonPollingDeviceCount += 1;
	nonPollingDevices = (DeviceFunction*)realloc(nonPollingDevices, nonPollingDeviceCount * sizeof(DeviceFunction));
	nonPollingDevices[id] = DeviceFunction{ id , data, deviceFunction, nullptr, nullptr };
	deviceFunction(data, id, *this);
	devices[id] = &nonPollingDevices[id];
	return id;
}

void InputManager::AddInputSettingHandler(
	DeviceID deviceId,
	void(*setSettingFunction)(void* data, DeviceID, SettingID, float value),
	float(*getSettingFunction)(void* data, DeviceID, SettingID))
{
	devices[deviceId]->setSettingFunction = setSettingFunction;
	devices[deviceId]->getSettingFunction = getSettingFunction;
}

void InputManager::SetSetting(DeviceID deviceID, SettingID settingID, float value)
{
	void* data = devices[deviceID]->data;
	devices[deviceID]->setSettingFunction(data, deviceID, settingID, value);
}

float InputManager::GetSetting(DeviceID deviceID, SettingID settingID)
{
	void* data = devices[deviceID]->data;
	return devices[deviceID]->getSettingFunction(data, deviceID, settingID);
}

float InputManager::GetRawInput(DeviceID deviceID, InputID inputID)
{
	Hash pair = getHash(deviceID, inputID);
	if (stored.find(pair) == stored.end())
		return 0.0f;
	return stored[pair];
}

float InputManager::GetRawInput(Hash hash)
{
	if (stored.find(hash) == stored.end())
		return 0.0f;
	return stored[hash];
}

void InputManager::AddVirtualInput(uint32_t VirtualKeyID, DeviceID deviceID, InputID inputID, float(*filter)(float))
{
	Hash pair = getHash(deviceID, inputID);
	virtualKeys[pair] = std::make_pair(VirtualKeyID, filter);
	virtualToRealKeys[VirtualKeyID] = getHash(deviceID, inputID);
}

float InputManager::GetVirtualInput(uint32_t VirtualKeyID)
{
	Hash hash = virtualToRealKeys[VirtualKeyID];
	auto virtualKey = virtualKeys[hash];
	if (virtualKey.second == nullptr)
		return GetRawInput(hash);
	return virtualKey.second(GetRawInput(hash));
}