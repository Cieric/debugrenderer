#pragma once
#include <typeinfo>
#include <vector>
#include <unordered_map>
#include <stdint.h>
#include "common/handle_map.h"

struct ComponentRef
{
	size_t type;
	griffin::Id_T handle;
};

class ComponentManager
{
	std::unordered_map<size_t, void*> componentMaps;
public:
	ComponentManager() {};

	template<typename T>
	griffin::handle_map<T>* getMap();

	template<typename T>
	ComponentRef insert(const T& component);

	template<typename T>
	T get(ComponentRef handle);
};

template<typename T>
inline griffin::handle_map<T>* ComponentManager::getMap()
{
	size_t key = typeid(T).hash_code();
	if (componentMaps.find(key) == componentMaps.end())
	{
		static uint16_t id = 0;
		void* temp = (void*)new griffin::handle_map<T>(id++, 1024);
		componentMaps.insert(std::make_pair(key, temp));
	}

	auto temp2 = componentMaps[key];
	return (griffin::handle_map<T>*)(temp2);
}

template<typename T>
inline ComponentRef ComponentManager::insert(const T& component)
{
	size_t key = typeid(T).hash_code();
	griffin::Id_T handle = getMap<T>()->insert(component);
	return ComponentRef{
		key,
		handle
	};
}

template<typename T>
inline T ComponentManager::get(ComponentRef handle)
{
	return *getMap<T>()[handle.handle];
}


