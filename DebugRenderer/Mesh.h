#pragma once
#include "tiny_obj_loader.h"
#include "Vertex.h"

class Mesh {
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	unsigned int VAO, VBO, EBO;
	int MatId = -1;
public:
	Mesh(tinyobj::attrib_t attrib, tinyobj::shape_t shape);
	void CleanUp();
	void Draw();
	
	inline int GetMaterialID()
	{
		return MatId;
	}
};