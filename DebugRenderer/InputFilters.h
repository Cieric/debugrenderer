#pragma once

namespace InputManagers
{
	template<int T>
	float delta(float x)
	{
		static float last = 0;
		float delta = x - last;
		last = x;
		return delta;
	}
}