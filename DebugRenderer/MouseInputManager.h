#pragma once
#include <GLFW/glfw3.h>
#include "Window.h"
#include "InputManager.h"

namespace InputManagers
{
	struct MouseInputManagerData
	{
		Window* window;
	};

	enum class MouseInputs : InputID
	{
		Button1 = 0,
		Button2 = 1,
		Button3 = 2,
		Button4 = 3,
		Button5 = 4,
		Button6 = 5,
		Button7 = 6,
		Button8 = 7,
		ScrollY = 8,
		ScrollX = 9,
		PosX	= 10,
		PosY	= 11,
		LeftButton = Button1,
		RightButton = Button2,
		MiddleButton = Button3
	};


	enum class MouseSettings : SettingID
	{
		LockMouse = 0,
	};

	static void MouseInputManager(void* _data, DeviceID _deviceID, InputManager& _inputManager)
	{
		static DeviceID deviceID = _deviceID;
		static InputManager& inputManager = _inputManager;
		MouseInputManagerData* data = (MouseInputManagerData*)_data;

		data->window->mouseButtonEvent += [](Window*, int button, int action, int mods)
		{
			if (action == GLFW_REPEAT)
				return;
			inputManager.InputUpdateCallback(deviceID, button, (action == GLFW_RELEASE) ? 0.0f : 1.0f);
		};

		data->window->mouseButtonEvent += [](Window* window, int button, int action, int mods)
		{
			if (action == GLFW_REPEAT)
				return;
			inputManager.InputUpdateCallback(deviceID, button, (action == GLFW_RELEASE) ? 0.0f : 1.0f);
		};

		data->window->scrollEvent += [](Window* window, double xoffset, double yoffset)
		{
			inputManager.InputUpdateCallback(deviceID, GLFW_MOUSE_BUTTON_LAST + 2, (float)xoffset);
			inputManager.InputUpdateCallback(deviceID, GLFW_MOUSE_BUTTON_LAST + 1, (float)yoffset);
		};

		data->window->cursorPosEvent += [](Window* window, double xpos, double ypos)
		{
			inputManager.InputUpdateCallback(deviceID, GLFW_MOUSE_BUTTON_LAST + 3, (float)xpos);
			inputManager.InputUpdateCallback(deviceID, GLFW_MOUSE_BUTTON_LAST + 4, (float)ypos);
		};
	};

	static void SetMouseSetting(void* _data, DeviceID deviceID, SettingID settingID, float value)
	{
		MouseInputManagerData* data = (MouseInputManagerData*)_data;
		switch ((MouseSettings)settingID)
		{
			case MouseSettings::LockMouse: {
				data->window->setInputMode(GLFW_CURSOR, (value==1.0f)? GLFW_CURSOR_DISABLED:GLFW_CURSOR_NORMAL);
			} break;
		}
	}

	static float GetMouseSetting(void* _data, DeviceID deviceID, SettingID settingID)
	{
		MouseInputManagerData* data = (MouseInputManagerData*)_data;
		switch ((MouseSettings)settingID)
		{
		case MouseSettings::LockMouse: {
			int mode = data->window->getInputMode(GLFW_CURSOR);
			if (mode == GLFW_CURSOR_DISABLED)
				return 1.0f;
			return 0.0f;
		} break;
		}
		return INFINITY;
	}
}