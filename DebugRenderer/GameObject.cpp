#include "GameObject.h"
#include "RenderComponent.h"
#include "DataManager.h"
#include <string>
#include <fstream>
#include <streambuf>

GameObject::GameObject(GameScene* scene, std::string name, std::string pathLoad) : scene(scene), name(name) {
	if (pathLoad == "")
		return;

	printf("Cannot load %s from file!\n", pathLoad.c_str());
}
