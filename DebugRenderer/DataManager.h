#pragma once
#include <string>

class Model;

class DataManager
{
private:
	DataManager();
	std::string basedir;

public:
	static DataManager& getInstance();

	void setBaseDir(const std::string basedir);
	Model* loadModel(const char* filepath);
};