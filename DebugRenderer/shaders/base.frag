#version 330 core
out vec4 FragColor;

in vec3 Normal;  
in vec3 FragPos;  
in vec2 Texcoord;

in vec3 viewPos;
  
//vec3 lightPos = vec3(1.0, 0.5, 0.31);
//vec3 viewPos = vec3(-FragPos);
vec3 lightColor = vec3(1.0);

uniform sampler2D tex;

void main()
{
	vec3 lightPos = viewPos;

	vec4 texColor = texture(tex, Texcoord * vec2(1, -1));
	if(texColor.a < 0.5)
		discard;

	vec3 objectColor = texColor.rgb;

    // ambient
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
    
    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;  
        
    vec3 result = (ambient + diffuse + specular) * objectColor;
    FragColor = vec4(result, texColor.a);
} 