#version 400 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;
uniform mat4 Proj;
uniform mat4 View;
uniform mat4 Model;

out vec3 Normal;
out vec3 FragPos;
out vec2 Texcoord;

out vec3 viewPos;

void main()
{
    FragPos = vec3(Model * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(Model))) * aNormal;  
    gl_Position = Proj * View * vec4(FragPos, 1.0);
	Texcoord = aTexCoord;
	viewPos = -transpose(mat3(View)) * View[3].xyz;
}