#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "Model.h"
#include "InputManager.h"
#include "KeyboardInputManager.h"
#include "MouseInputManager.h"
#include "DataManager.h"
#include "InputFilters.h"
#include "Shader.h"
#include "RenderComponent.h"
#include "GameObject.h"
#include "GameScene.h"
#include "Window.h"

using namespace InputManagers;

void processInput(InputManager& inputManager);

glm::vec4 pos(0,0,0,1);
float v = 0, h = 0;
bool quit = false;
glm::mat4 view = glm::mat4(
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
);

enum VirtualKeys : uint32_t
{
	MoveForward,
	MoveBackward,
	MoveLeft,
	MoveRight,
	MoveUp,
	MoveDown,
	HookCamera,
	Quit,
	LookVertical,
	LookHorizontal
};

DeviceID keyboardDevice;
DeviceID mouseDevice;

Shader shader;
glm::mat4 proj;

void framebufferSizeCallback(Window* window, int width, int height)
{
	float fov = 81.0f;
	float S = 1.0f / tan((fov / 2.0f) * (3.14159f / 180.0f));
	float f = 100.0f;
	float n = 0.1f;
	proj = {
		S * (float)height / (float)width,0,0,0,
		0,S,0,0,
		0,0,-f / (f - n),-1,
		0,0,-(f * n) / (f - n),0
	};

	shader.SetUniformValue("Proj", &proj[0]);
	glViewport(0, 0, width, height);
}

int main()
{
	Window window(800, 600);
	window.framebufferSizeEvent += framebufferSizeCallback;

	{
		float fov = 81.0f;
		float S = 1.0f / tan((fov / 2.0f) * (3.14159f / 180.0f));
		float f = 100.0f;
		float n = 0.1f;
		proj = {
			S*(float)window.getHeight()/(float)window.getWidth(),0,0,0,
			0,S,0,0,
			0,0,-f / (f - n),-1,
			0,0,-(f * n) / (f - n),0
		};
	}

	shader = Shader("shaders/base.node", false);
	shader.SetUniformValue("Proj", &proj[0]);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);

	DataManager& dm = DataManager::getInstance();
	dm.setBaseDir("models/");

	GameScene* scene = new GameScene();

	GameObject Map(scene, "Map");
	Map.transform.scale = glm::vec3(100);
	Map.AddComponent(RenderComponent(dm.loadModel("TestMap/TerrainTestMap.mdl"), &shader));

	Model* TreeModel = dm.loadModel("Block/Block.mdl");

	std::vector<GameObject> objs;

	for (int x = 0; x < 12; x += 1)
	{
		for (int y = 0; y < 12; y += 1)
		{
			GameObject* Tree1 = new GameObject(scene, "Tree1");
			Tree1->transform.pos = glm::vec3(x * 2, 0, y * 2);
			Tree1->AddComponent(RenderComponent(TreeModel, &shader));
			objs.push_back(std::move(*Tree1));
		}
	}

	InputManager IM;
	keyboardDevice = IM.AddNonPollingDeviceFunction(new KeyboardInputManagerData{ &window }, KeyboardInputManager);
	mouseDevice = IM.AddNonPollingDeviceFunction(new MouseInputManagerData{ &window }, MouseInputManager);
	IM.AddInputSettingHandler(mouseDevice, SetMouseSetting, GetMouseSetting);

	IM.AddVirtualInput(MoveForward, keyboardDevice, (InputID)KeyboardInputs::KeyW, nullptr);
	IM.AddVirtualInput(MoveBackward, keyboardDevice, (InputID)KeyboardInputs::KeyS, nullptr);
	IM.AddVirtualInput(MoveLeft, keyboardDevice, (InputID)KeyboardInputs::KeyA, nullptr);
	IM.AddVirtualInput(MoveRight, keyboardDevice, (InputID)KeyboardInputs::KeyD, nullptr);
	IM.AddVirtualInput(MoveUp, keyboardDevice, (InputID)KeyboardInputs::KeySpace, nullptr);
	IM.AddVirtualInput(MoveDown, keyboardDevice, (InputID)KeyboardInputs::KeyLShift, nullptr);
	IM.AddVirtualInput(Quit, keyboardDevice, (InputID)KeyboardInputs::KeyESC, delta<Quit>);

	IM.AddVirtualInput(LookVertical, mouseDevice, (InputID)MouseInputs::PosY, delta<LookVertical>);
	IM.AddVirtualInput(LookHorizontal, mouseDevice, (InputID)MouseInputs::PosX, delta<LookHorizontal>);
	IM.AddVirtualInput(HookCamera, mouseDevice, (InputID)MouseInputs::RightButton, delta<HookCamera>);

	double previousTime = glfwGetTime();
	int frameCount = 0;

	while (!window.shouldClose() && !quit)
	{
		double currentTime = glfwGetTime();
		frameCount++;
		if (currentTime - previousTime >= 1.0)
		{
			char buffer[32] = {};
			sprintf_s(buffer, "FPS: %d", frameCount);
			window.setWindowTitle(buffer);
			frameCount = 0;
			previousTime = currentTime;
		}

		processInput(IM);
		glClearDepth(1.0);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		float time = (float)currentTime;
		//shader.SetUniformValue("time", &time);
		shader.Use();
		for (RenderComponent comp : *(scene->GetComponentManager()).getMap<RenderComponent>())
			comp.Draw();

		window.swapBuffers();
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

void processInput(InputManager& inputManager)
{
	if (inputManager.GetSetting(mouseDevice, (SettingID)MouseSettings::LockMouse) == 1.0f)
	{
		v += inputManager.GetVirtualInput(LookVertical) * 0.05f;
		h += inputManager.GetVirtualInput(LookHorizontal) * 0.05f;
		if (v > 90.f) v = 90.f;
		if (v < -90.f) v = -90.f;
		glm::mat4 dir = glm::rotate(glm::rotate(glm::mat4(1), glm::radians(v), glm::vec3(1, 0, 0)), glm::radians(h), glm::vec3(0, 1, 0));
		glm::vec4 move = glm::vec4(
			inputManager.GetVirtualInput(MoveLeft) - inputManager.GetVirtualInput(MoveRight),
			inputManager.GetVirtualInput(MoveDown) - inputManager.GetVirtualInput(MoveUp),
			inputManager.GetVirtualInput(MoveForward) - inputManager.GetVirtualInput(MoveBackward),
			0
		);
		float speed = 0.1f;
		pos += (move * dir) * speed;
		view = glm::translate(dir, glm::vec3(pos.x, pos.y, pos.z));
		shader.SetUniformValue("View", &view[0][0]);

		if (inputManager.GetVirtualInput(Quit) == 1.0f)
			inputManager.SetSetting(mouseDevice, (SettingID)MouseSettings::LockMouse, 0.0f);
	}
	else
	{
		if (inputManager.GetVirtualInput(HookCamera) == 1.0f)
		{
			inputManager.GetVirtualInput(LookVertical);
			inputManager.GetVirtualInput(LookHorizontal);
			inputManager.SetSetting(mouseDevice, (SettingID)MouseSettings::LockMouse, 1.0f);
		}
		if (inputManager.GetVirtualInput(Quit) == 1.0f)
			quit = true;
	}
}