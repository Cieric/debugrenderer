#pragma once
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include "IComponent.h"
#include "ComponentManager.h"
#include "Transform.h"
#include "GameScene.h"

class GameObject
{
	std::string name;
	std::vector<ComponentRef> components;
	GameScene* scene;

public:
	Transform transform;
	GameObject(GameScene* scene, std::string name, std::string pathLoad = "");

	template<typename T>
	void AddComponent(T comp);
};

template<typename T>
void GameObject::AddComponent(T comp)
{
	ComponentManager& cm = scene->GetComponentManager();
	(*(IComponent*)& comp).parent = this;
	components.push_back(std::move(cm.insert(comp)));
}