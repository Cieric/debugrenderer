#include "Utils.h"
#include <fstream>

size_t loadFile(char*& output, const char* filepath)
{
	std::ifstream file(filepath, std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		std::streamoff size = file.tellg();
		output = (char*)malloc(size + 1);
		if (output == nullptr)
			return false;
		file.seekg(0, std::ios::beg);
		file.read(output, size);
		output[size] = 0;
		file.close();
		return size;
	}
	return 0;
}

std::string getBaseDir(const std::string path)
{
	size_t idx = path.rfind('/');

	if (idx == std::string::npos)
		return "";
	return path.substr(0, idx+1);
}


std::string getFileName(const std::string path)
{
	size_t idx = path.rfind('/');

	if (idx == std::string::npos)
		return path;
	return path.substr(idx + 1);
}

std::string getFileExtention(const std::string path)
{
	std::string filename = getFileName(path);
	size_t idx = filename.rfind('.');
	if (idx == std::string::npos)
		return "";
	return filename.substr(idx + 1);
}

const char * UniformTypeToString(GLenum type)
{
	switch (type)
	{
	case GL_FLOAT: return "GL_FLOAT";
	case GL_FLOAT_MAT4: return "GL_FLOAT_MAT4";
	}
	return "Unknown";
}

uint32_t UniformTypeSize(GLenum type)
{
	switch (type)
	{
	case GL_FLOAT: return sizeof(float);
	case GL_FLOAT_MAT4: return sizeof(float) * 16;
	}
	return 0;
}
