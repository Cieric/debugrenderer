#pragma once
#include <string>

class Node
{
	std::string path;
	const char* data;
	size_t offset = 0;
public:
	Node(std::string path);

	template<typename T>
	T get();
};

template<typename T>
inline T Node::get() {
	T* temp = (T*)(data + offset);
	offset += sizeof(T);
	return *temp;
}

template<>
inline std::string Node::get() {
	uint32_t len = get<uint32_t>();
	std::string temp = std::string(data + offset, data + offset + len);
	offset += len;
	return std::move(temp);
}
