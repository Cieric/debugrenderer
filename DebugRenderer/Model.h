#pragma once
#include <vector>
#include "Texture.h"
#include "Vertex.h"
#include "Mesh.h"

class Model
{
public:
	std::vector<Mesh> meshes;
	std::vector<Texture> textures;
	Model();
	void Draw();
	void CleanUp();
};