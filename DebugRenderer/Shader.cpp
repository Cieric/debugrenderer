#include <iostream>
#include "Shader.h"
#include "Utils.h"
#include "NodeLoader.h"
#include <glad/glad.h>

bool loadShader(const char* file, int shaderType, uint32_t& id)
{
	char* shaderSource = nullptr;
	if (!loadFile(shaderSource, file))
	{
		printf("failed to open file (%s)!\n", file);
		return false;
	}

	unsigned int shader;
	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderSource, NULL);
	glCompileShader(shader);
	int  success; char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	free(shaderSource);
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		printf("ERROR::SHADER::COMPILATION_FAILED\n%s\n", infoLog);
		return false;
	}

	id = shader;
	return true;
}

void Shader::Recompile()
{
	glDeleteShader(program);
	program = glCreateProgram();
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);

	int success;
	char infoLog[512];
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
		program = 0;
		return;
	}

	GLint uniformCount;
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &uniformCount);
	//printf("Uniform Count: %d\n", uniformCount);
	uint32_t UniformSize = 0;
	std::unordered_map<GLint, Uniform> tempUniforms = uniforms;
	uniforms.clear();

	char buffer[64];
	for (int i = 0; i < uniformCount; ++i)
	{
		GLint length;
		GLenum type;

		glGetActiveUniform(program, i, 64, nullptr, &length, &type, buffer);
		//printf("%s %s[%d]\n", UniformTypeToString(type), buffer, length);
		GLint loc = glGetUniformLocation(program, buffer);
		uint32_t dataSize = UniformTypeSize(type) * length;
		uniforms[loc] = std::move(Uniform{ buffer, dataSize, UniformSize, type, loc, length });
		UniformSize += dataSize;
	}

	void* OldUniformBuffer = UniformBuffer;
	UniformBuffer = malloc(UniformSize);

	for (std::pair<GLint, Uniform> pair1 : tempUniforms)
	{
		for (std::pair<GLint, Uniform> pair2 : uniforms)
		{
			if (pair1.second.name == pair2.second.name)
			{
				float* data = (float*)((char*)OldUniformBuffer + pair1.second.offset);
				SetUniformValue(pair1.second.name.c_str(), data);
			}
		}
	}

	if (OldUniformBuffer) free(OldUniformBuffer);
}

Shader::Shader(std::string path, bool autoUpdate) : autoUpdate(autoUpdate), nodePath(path)
{
	Node node(nodePath);
	dir = getBaseDir(nodePath);
	vertPath = dir + "/" + node.get<std::string>();
	fragPath = dir + "/" + node.get<std::string>();

	loadShader(vertPath.c_str(), GL_VERTEX_SHADER, vertShader);
	loadShader(fragPath.c_str(), GL_FRAGMENT_SHADER, fragShader);

	Recompile();

	if (!autoUpdate)
	{
		glDeleteShader(vertShader);
		vertShader = 0;
		glDeleteShader(fragShader);
		fragShader = 0;
	}
	else
	{
		HANDLE hndl = CreateFileA(vertPath.c_str(), GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		GetFileTime(hndl, nullptr, nullptr, &lastUpdate);
		CloseHandle(hndl);
		FILETIME temp;
		hndl = CreateFileA(fragPath.c_str(), GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		GetFileTime(hndl, nullptr, nullptr, &temp);
		CloseHandle(hndl);
		if (*(uint64_t*)&lastUpdate < *(uint64_t*)&temp)
			lastUpdate = temp;
	}
}

void Shader::Update()
{
	if (!autoUpdate) return;
	FILETIME temp;
	bool recompileFlag = false;

	HANDLE hndl = CreateFileA(vertPath.c_str(), GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	GetFileTime(hndl, nullptr, nullptr, &temp);
	CloseHandle(hndl);
	uint64_t tempTime = *(uint64_t*)&lastUpdate;
	if (tempTime < *(uint64_t*)&temp && *(uint64_t*)&temp != 14757395258967641292ULL)
	{
		//printf("reloading vertex shader!\n");
		uint32_t tempShader = 0;
		if (loadShader(vertPath.c_str(), GL_VERTEX_SHADER, tempShader))
		{
			printf("Vertex shader reloaded!\n");
			glDeleteShader(vertShader);
			vertShader = tempShader;
			recompileFlag = true;
		}
		else
		{
			printf("Vertex shader failed to load!\n");
		}
		if(*(uint64_t*)&lastUpdate < *(uint64_t*)&temp)
			lastUpdate = temp;
	}

	hndl = CreateFileA(fragPath.c_str(), GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	GetFileTime(hndl, nullptr, nullptr, &temp);
	CloseHandle(hndl);
	if (tempTime < *(uint64_t*)&temp && *(uint64_t*)&temp != 14757395258967641292ULL)
	{
		//printf("reloading fragment shader!\n");
		uint32_t tempShader = 0;
		if (loadShader(fragPath.c_str(), GL_FRAGMENT_SHADER, tempShader))
		{
			printf("Fragment shader reloaded!\n");
			glDeleteShader(fragShader);
			fragShader = tempShader;
			recompileFlag = true;
		}
		else
		{
			printf("Fragment shader failed to load!\n");
		}
		if (*(uint64_t*)&lastUpdate < *(uint64_t*)&temp)
			lastUpdate = temp;
	}

	if (recompileFlag)
	{
		Recompile();
	}
}

void Shader::Use()
{
	if (autoUpdate) Update();
	glUseProgram(program);
}

void Shader::SetUniformValue(const char * str, const void * data)
{
	GLint loc = GetUniformLocation(str);
	Uniform& uniform = uniforms[loc];
	void* dest = (char*)UniformBuffer + uniform.offset;
	memcpy(dest, data, uniform.size);
	Use();
	switch (uniform.type)
	{
	case GL_FLOAT:
		glUniform1f(loc, *(GLfloat*)dest);
		break;

	case GL_FLOAT_MAT4:
		glUniformMatrix4fv(loc, uniform.length, false, (GLfloat*)dest);
		break;
	}
}

GLint Shader::GetUniformLocation(const char* str)
{
	if (uniformLocation.find(str) == uniformLocation.end())
		uniformLocation.insert(std::make_pair(std::string(str), glGetUniformLocation(program, str)));
	return uniformLocation[str];
}
