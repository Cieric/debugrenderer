#pragma once

class GameObject;

struct IComponent
{
	GameObject* parent;
};