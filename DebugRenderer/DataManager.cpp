#include "DataManager.h"
#include "ModelLoader.h"
#include "Utils.h"

DataManager::DataManager()
{
}

DataManager& DataManager::getInstance()
{
	static DataManager* instance = new DataManager();
	return *instance;
}

void DataManager::setBaseDir(const std::string basedir)
{
	this->basedir = basedir;
}

Model* DataManager::loadModel(const char* filepath)
{
	std::string filename = getFileName(filepath);

	return ModelLoader::getInstance().loadModel(
		filename.c_str(),
		(this->basedir + getBaseDir(filepath)).c_str()
	);
}
