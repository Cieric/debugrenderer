#pragma once
#include <stdint.h>
#include <vector>
#include <unordered_map>

typedef uint16_t DeviceID;
typedef uint16_t InputID;
typedef uint16_t SettingID;
typedef uint32_t Hash;

class InputManager
{
	struct DeviceFunction
	{
		DeviceID id;
		void* data;
		void(*deviceFunction)(void* data, DeviceID, InputManager&);
		void(*setSettingFunction)(void* data, DeviceID, SettingID, float value);
		float(*getSettingFunction)(void* data, DeviceID, SettingID);
	};

	uint32_t pollingDeviceCount = 0;
	DeviceFunction* pollingDevices = nullptr;
	uint32_t nonPollingDeviceCount = 0;
	DeviceFunction* nonPollingDevices = nullptr;
	std::unordered_map<DeviceID, DeviceFunction*> devices;
	std::unordered_map<Hash, float> stored;
	std::unordered_map<uint32_t, std::pair<Hash, float(*)(float)>> virtualKeys;
	std::unordered_map<uint32_t, Hash> virtualToRealKeys;
	Hash getHash(DeviceID did, InputID iid);
public:
	void InputUpdateCallback(DeviceID deviceID, InputID inputID, float val);
	void Update();
	DeviceID AddNonPollingDeviceFunction(void *data, void(*deviceFunction)(void* data, DeviceID, InputManager&));
	void AddInputSettingHandler(DeviceID deviceId,
		void(*setSettingFunction)(void* data, DeviceID, SettingID, float value), 
		float(*getSettingFunction)(void* data, DeviceID, SettingID));
	void SetSetting(DeviceID, SettingID, float);
	float GetSetting(DeviceID, SettingID);
	float GetRawInput(DeviceID, InputID);
	float GetRawInput(Hash hash);
	void AddVirtualInput(uint32_t VirtualKeyID, DeviceID deviceID, InputID inputID, float (*filter)(float));
	float GetVirtualInput(uint32_t VirtualKeyID);
};