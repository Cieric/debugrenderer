#include "Mesh.h"
#include <glad/glad.h>

Mesh::Mesh(tinyobj::attrib_t attrib, tinyobj::shape_t shape)
{
	unsigned int Count = 0;
	size_t index_offset = 0;
	for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++) {
		size_t fv = shape.mesh.num_face_vertices[f];
		for (size_t v = 0; v < fv; v++) {
			tinyobj::index_t idx = shape.mesh.indices[index_offset + v];
			vertices.push_back(Vertex{
				glm::vec3(attrib.vertices[3LL * idx.vertex_index + 0], attrib.vertices[3LL * idx.vertex_index + 1], attrib.vertices[3LL * idx.vertex_index + 2]),
				glm::vec3(attrib.normals[3LL * idx.normal_index + 0], attrib.normals[3LL * idx.normal_index + 1], attrib.normals[3LL * idx.normal_index + 2]),
				glm::vec2(attrib.texcoords[2LL * idx.texcoord_index + 0], attrib.texcoords[2LL * idx.texcoord_index + 1])
			});
			indices.push_back(Count++);
		}
		index_offset += fv;
	}

	MatId = (shape.mesh.material_ids.size() > 0) ? shape.mesh.material_ids[0] : 0;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

	glBindVertexArray(0);
}

void Mesh::CleanUp()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
}

void Mesh::Draw()
{
	glBindVertexArray(VAO);
	//glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
