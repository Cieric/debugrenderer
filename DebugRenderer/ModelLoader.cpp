#include "ModelLoader.h"
#include "Model.h"
#include "Utils.h"
#include "lodepng.h"
#include <iostream>

ModelLoader::ModelLoader()
{
}

ModelLoader& ModelLoader::getInstance()
{
	static ModelLoader* instance = new ModelLoader();
	return *instance;
}

Model* ModelLoader::loadModel(const char* filepath, const char* basedir)
{
	Model* model = new Model();

	std::string extention = getFileExtention(filepath);

	if (extention == "mdl")
	{
		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;

		std::string warn;
		std::string err;

		char buffer[256] = {};
		strcpy_s(buffer, basedir);
		strcat_s(buffer, filepath);

		std::string ext = getFileExtention(buffer);

		bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, buffer, basedir);

		if (!err.empty()) {
			std::cerr << err << std::endl;
		}

		if (!warn.empty()) {
			std::cerr << warn << std::endl;
		}

		for (size_t s = 0; s < shapes.size(); s++) {
			model->meshes.push_back(Mesh(attrib, shapes[s]));
		}

		for (size_t m = 0; m < materials.size(); m++)
		{
			tinyobj::material_t& mat = materials[m];
			if (mat.diffuse_texname[0] != 0)
			{
				std::vector<unsigned char> image;
				unsigned width, height;
				unsigned error = lodepng::decode(image, width, height, std::string(basedir) + mat.diffuse_texname);
				Texture tex = { 0, "Diffuse" };
				glGenTextures(1, &tex.id);
				glBindTexture(GL_TEXTURE_2D, tex.id);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
				glBindTexture(GL_TEXTURE_2D, 0);
				model->textures.push_back(tex);
			}
		}
	}
	else if (extention == "cmdl")
	{

	}
	else
	{
		std::cerr << "file extention not parsable (" << extention << ")" << std::endl;
	}

	return model;
}
