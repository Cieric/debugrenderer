#pragma once
#include <unordered_map>
#include <glad/glad.h>
#include <windows.h>

struct Uniform
{
	std::string name;
	size_t size;
	uint32_t offset;
	GLenum type;
	GLint loc;
	GLint length;
};

class Shader
{
	FILETIME lastUpdate;
	uint32_t vertShader = 0;
	uint32_t fragShader = 0;
	uint32_t program = 0;
	void* UniformBuffer = nullptr;
	bool autoUpdate;

	std::string dir;
	std::string nodePath;
	std::string fragPath;
	std::string vertPath;

	std::unordered_map<GLint, Uniform> uniforms;
	std::unordered_map<std::string, GLint> uniformLocation;

	void Recompile();
public:
	Shader() = default;
	Shader(std::string path, bool autoUpdate);
	void Update();
	void Use();
	void SetUniformValue(const char* str, const void* data);
	GLint GetUniformLocation(const char* str);
};