#include "Window.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>

void Window::framebufferSizeCallback(int width, int height)
{
	this->width = width;
	this->height = height;
	this->framebufferSizeEvent(this, width, height);
}

void Window::keyCallback(int key, int scancode, int action, int mods)
{
	this->keyEvent(this, key, scancode, action, mods);
}

void Window::mouseButtonCallback(int button, int action, int mods)
{
	this->mouseButtonEvent(this, button, action, mods);
}

void Window::scrollCallback(double xoffset, double yoffset)
{
	this->scrollEvent(this, xoffset, yoffset);
}

void Window::cursorPosCallback(double xpos, double ypos)
{
	this->cursorPosEvent(this, xpos, ypos);
}

Window::Window(int width, int height): width(width), height(height)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		throw "Failed to create GLFW window";
	}
	glfwMakeContextCurrent(window);
	glfwSetWindowUserPointer(window, this);
	glfwSetFramebufferSizeCallback(window,
		[](GLFWwindow* window, int width, int height)->void {
			static_cast<Window*>(glfwGetWindowUserPointer(window))->framebufferSizeCallback(width, height);
		});

	glfwSetKeyCallback(window,
		[](GLFWwindow* window, int key, int scancode, int action, int mods)->void {
			static_cast<Window*>(glfwGetWindowUserPointer(window))->keyCallback(key, scancode, action, mods);
		});

	glfwSetMouseButtonCallback(window,
		[](GLFWwindow* window, int button, int action, int mods)->void {
			static_cast<Window*>(glfwGetWindowUserPointer(window))->mouseButtonCallback(button, action, mods);
		});

	glfwSetScrollCallback(window,
		[](GLFWwindow* window, double xoffset, double yoffset)->void {
			static_cast<Window*>(glfwGetWindowUserPointer(window))->scrollCallback(xoffset, yoffset);
		});

	glfwSetCursorPosCallback(window,
		[](GLFWwindow* window, double xoffset, double yoffset)->void {
			static_cast<Window*>(glfwGetWindowUserPointer(window))->cursorPosCallback(xoffset, yoffset);
		});

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		throw "Failed to initialize GLAD";
	}
	
}

int Window::getWidth()
{
	return width;
}

int Window::getHeight()
{
	return height;
}

bool Window::shouldClose()
{
	return glfwWindowShouldClose(window);
}

void Window::setInputMode(int mode, int value)
{
	return glfwSetInputMode(window, mode, value);
}

int Window::getInputMode(int mode)
{
	return glfwGetInputMode(window, mode);
}

void Window::setWindowTitle(const char* title)
{
	return glfwSetWindowTitle(window, title);
}

void Window::swapBuffers()
{
	return glfwSwapBuffers(window);
}
