#pragma once
#include <glm/glm.hpp>
//#include <glm/detail/type_quat.hpp>
#include <glm/gtc/quaternion.hpp>

struct Transform
{
	glm::vec3 pos = glm::vec3(0);
	glm::fquat rotation = glm::fquat();
	glm::vec3 scale = glm::vec3(1);

	glm::mat4 GetMatrix()
	{
		glm::mat4 temp = glm::mat4(1.0f);
		temp = glm::translate(temp, pos);
		temp = temp * glm::mat4_cast(rotation);
		return glm::scale(temp, scale);
	}
};