#pragma once
#include <GLFW/glfw3.h>
#include <unordered_map>
#include "Event.h"
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

class Window
{
	GLFWwindow* window;
	int width = 0;
	int height = 0;
	void framebufferSizeCallback(int width, int height);
	void keyCallback(int key, int scancode, int action, int mods);
	void mouseButtonCallback(int button, int action, int mods);
	void scrollCallback(double xoffset, double yoffset);
	void cursorPosCallback(double xpos, double ypos);
public:

	Event<Window*, int, int> framebufferSizeEvent;
	Event<Window*, int, int, int, int> keyEvent;
	Event<Window*, int, int, int> mouseButtonEvent;
	Event<Window*, double, double> scrollEvent;
	Event<Window*, double, double> cursorPosEvent;

	Window(int width, int height);
	~Window() = default;

	int getWidth();
	int getHeight();

	bool shouldClose();
	void setInputMode(int mode, int value);
	int getInputMode(int mode);
	void setWindowTitle(const char* title);
	void swapBuffers();
};