#include "Model.h"
#include <glad/glad.h>
#include <iostream>
#include "tiny_obj_loader.h"
#include "lodepng.h"
#include "Utils.h"

Model::Model()
{
}

void Model::Draw()
{
	for (Mesh& mesh : meshes)
	{
		int id = mesh.GetMaterialID();
		if(id >= 0)
			glBindTexture(GL_TEXTURE_2D, textures[id].id);
		mesh.Draw();
	}
}

void Model::CleanUp()
{
}
