#pragma once
#include <stdint.h>
#include <string>
#include <glad/glad.h>

size_t loadFile(char*& output, const char* filepath);

std::string getBaseDir(const std::string path);
std::string getFileName(const std::string path);
std::string getFileExtention(const std::string path);

const char* UniformTypeToString(GLenum type);
uint32_t UniformTypeSize(GLenum type);