#pragma once
#include "IComponent.h"
#include "Model.h"
#include "Shader.h"

struct RenderComponent : IComponent
{
	Model* model;
	Shader* shader;

public:
	RenderComponent(Model* model, Shader* shader) : model(model), shader(shader) {};
	void Draw();
};