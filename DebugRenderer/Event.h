#pragma once
#include <vector>

template<typename ...C>
class Event
{
public:
	Event();
	~Event();

	void operator+=(void(*action)(C...));
	void operator-=(void(*action)(C...));
	void operator()(C... obj);

private:
	typedef std::vector<void(*)(C...)> CallbackArray;
	CallbackArray actions;
};

template<typename ... C>
Event<C...>::Event() { }

template<typename ... C>
Event<C...>::~Event() { }

template<typename ... C>
void Event<C...>::operator+=(void(*action)(C...))
{
	auto position = std::find(actions.begin(), actions.end(), action);

	if (position != actions.end())
	{
		puts("Action existed in delegate list.");
		return;
	}
	actions.push_back(action);
}

template<typename ...C>
void Event<C...>::operator-=(void(*action)(C...))
{
	auto position = std::find(actions.begin(), actions.end(), action);
	if (position == actions.end())
	{
		return;
	}
	actions.erase(position);
}

template<typename ...C>
void Event<C...>::operator()(C... obj)
{
	for (void(*action)(C...) : actions)
	{
		(*action)(obj...);
	}
}