#pragma once
#include "ComponentManager.h"

class GameScene
{
	ComponentManager componentManager;
public:
	GameScene();
	ComponentManager& GetComponentManager();
};