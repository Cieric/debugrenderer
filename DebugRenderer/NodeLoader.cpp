#include "NodeLoader.h"

Node::Node(std::string path) : path(path)
{
	FILE* file = nullptr;
	fopen_s(&file, path.c_str(), "r+");
	if (file == NULL)
		throw "File not found!";
	fseek(file, 0, SEEK_END);
	long int size = ftell(file);
	fseek(file, 0, SEEK_SET);
	data = (char*)malloc(size);
	if(data == nullptr)
		throw "Couldn't allocate memory!";
	(void)fread((void*)data, sizeof(char), size, file);
	fclose(file);
}
